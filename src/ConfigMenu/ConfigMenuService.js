export default {
    isVisible: false,
    listener: null,
    show() {
        this.isVisible = true;
        this.notify();
    },
    hide() {
        this.isVisible = false;
        this.notify();
    },
    toggle() {
        this.isVisible = !this.isVisible;
        this.notify();
    },
    notify() {
        if (this.listener) {
            this.listener(this.isVisible);
        }
    },
    addListener(listener) {
        this.listener = listener;
    },
}