import App from './App.html';
import 'material-design-lite/material.min.css';
import 'material-design-lite/material.min.js';
import ConfigMenuService from './ConfigMenu/ConfigMenuService.js';
import './Game/GameController.js';

const app = new App({
	target: document.body
});

window.app = app;

requestAnimationFrame(() => {
	ConfigMenuService.show();
});

export default app;