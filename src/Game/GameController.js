import { WasmBuilder } from '../wasmBuilder.js';
import SnackStore from '../Snack/SnackStore.js';
import GameStore from './GameStore.js';

const wBuilder = new WasmBuilder();
let remainingIterations = 100 * 100;

wBuilder.fetchWasm()
    .then(_ => setTimeout(() => SnackStore.sendToast('Done loading wasm!'), 500));

GameStore.on('state', ({ changed, current, previous }) => {
    if (changed.config) {
        remainingIterations = 100 * 100;
        wBuilder.start(parseInt(current.config.seed, 10), current.config.controller);
        update();
    }
});

function update() {
    const state = wBuilder.getState();
    GameStore.set({
        remaining: remainingIterations,
        body: state.body,
        berry: state.berry,
    });

    const fitness = wBuilder.step();
    console.log('Iter fitness:', fitness);
    if (remainingIterations-- > 0 && fitness == -1) {
        setTimeout(update, window.visSpeed || 1000);
    } else {
        SnackStore.sendToast(`Finished Playing! Fitness: ${fitness}`);
    }
}
