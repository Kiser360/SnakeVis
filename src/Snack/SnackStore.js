import { Store } from 'svelte/store.js';

class SnackStore extends Store {
    sendToast(message) {
        this.set({ message });
    }
};

export default new SnackStore();