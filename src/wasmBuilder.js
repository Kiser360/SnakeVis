export class WasmBuilder {

    constructor() {
        this.WASM_PATH = 'main.wasm';
        this.instance = null;
    }

    fetchWasm() {
        return fetch(this.WASM_PATH)
            .then(response => response.arrayBuffer())
            .then(bytes => WebAssembly.instantiate(bytes, {}))
            .then(results => this.instance = results.instance);
    }

    start(seed, controller) {
        this.instance.exports.init(seed);
        this.pushController(controller);
        this.instance.exports.start();
    }

    getState() {
        let result = {
            berry: { x: this.instance.exports.getBerryX(), y: this.instance.exports.getBerryY() },
            length: this.instance.exports.getLength(),
            body: []
        };

        for (let j = 0; j < result.length; j++) {
            result.body.unshift({
                x: this.instance.exports.getBodyX(j),
                y: this.instance.exports.getBodyY(j)
            });
        }

        return result;
    }

    step() {
        return this.instance.exports.update();
    }

    pushController(controller) {
        if (controller.slice(-1) !== '=') {
            controller = controller + ' =';
        }

        controller.split(' ')
            .filter(c => c !== '')
            .forEach(c => {
                console.log('Pushing: ' + c);
                if (!isNaN(c)) {
                    this.instance.exports.pushFloat(Number(c));
                } else {
                    this.instance.exports.pushByte(c.charCodeAt(0));
                }
            });
    }
}